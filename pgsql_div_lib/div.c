#include <string.h>
#include <float.h>
#include "postgres.h"
#include "fmgr.h"


#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

PG_FUNCTION_INFO_V1(divide);

#define TEXT_MAX_SIZE 20

Datum divide(PG_FUNCTION_ARGS) {
    int32 n1 = PG_GETARG_INT32(0);
    int32 n2 = PG_GETARG_INT32(1);

    text *res = (text*)palloc(VARHDRSZ + TEXT_MAX_SIZE);
    SET_VARSIZE(res, VARHDRSZ + TEXT_MAX_SIZE);

    if (n2 == 0) {
        snprintf((void*)VARDATA(res), TEXT_MAX_SIZE, "%f", n1>0 ? DBL_MAX : -DBL_MAX);
    } else {
        snprintf((void*)VARDATA(res), TEXT_MAX_SIZE, "%f", n1/((double)n2));
    }

    PG_RETURN_TEXT_P(res);
}
