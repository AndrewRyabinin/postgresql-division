#include <stdio.h>
#include "libpq-fe.h"

#define COUNT_OF(x) ((sizeof(x)/sizeof(0[x])) / ((size_t)(!(sizeof(x) % sizeof(0[x])))))

int main(int argc, char **argv) {
    PGconn *conn = NULL;
    PGresult *res;
    int rc = 1;

    if (argc != 3) {
        fprintf(stderr, "usage : %s <n1> <n2>\n\tn1 - numerator\n\tn2 - denominator\n", argv[0]);
        goto exit;
    }
    int n1 = atoi(argv[1]);
    int n2 = atoi(argv[2]);

    conn = PQconnectdb("user=postgres");
    if (PQstatus(conn) != CONNECTION_OK) {
        fprintf(stderr, "Connection to database failed: %s\n",
                PQerrorMessage(conn));
        goto exit;
    }

    char query[100];
    snprintf(query, COUNT_OF(query), "select divide(%d,%d)", n1, n2);

    res = PQexec(conn, query);
    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        fprintf(stderr, "%s command failed: %s\n",
                query,
                PQerrorMessage(conn));
        PQclear(res);
        goto exit;
    }
    
    printf ("%s\n", PQgetvalue(res,0,0));
    PQclear(res);
    rc = 0;

 exit:
    PQfinish(conn);
    return rc;
}
